# Tmux Quick Reference

## Sessions
### Start a new session
$ tmux
$ tmux new
$ tmux new_session 
:new

### Start a new session with the name mySession
$ tmux new -s mySesssion     
:new -s mySession

#### Kill/delete session mySession
$ tmux kill-ses -t mySession
$ tmux kill-session -t mySession

#### Kill/delete all sessions but current
$ tmux kill-session -a

#### Kill/delete all sessions but mySession
$ tmux kill-session -a -t mySession

### Rename Session
<Ctrl + b>  $

### Detach from Session
<Ctrl + b> d

### Show all sessions
$ tmux ls
$ tmux list-sessions
<Ctrl + b> s

### Attach to last session
$ tmux a
$ tmux at
$ tmux attach
$ tmux attach-session

### Attach to a session with the name mySession
$ tmux a -t mySession
$ tmux at -t mySession
$ tmux attach -t mySession
$ tmux attach-session -t mySession

### Move to the previous session
<Ctrl + b> (

### Move to the next session
<Ctrl + b> )


## Windows
#### Start a new session with the name mySession and window myWindow
$ tmux new -s mySession -n myWindows

### Create a new window
<Ctrl + b> c

### Rename current window
<Ctrl + b> ,

### Close current window
<Ctrl + b> &

### Previous window
<Ctrl + b> p 

### Next window 
<Ctrl + b> n

#### Switch or select window by numbers 
<Ctrl + b> 0...9

#### Reorder window, swap window number 2(source) and 1(destination)
:swap-window -s 2 -t 1

#### Move the current window to the left by one position
:swap-window -t -1

## Panes
### Toggle last active pane
<Ctrl + b> ;

### Split pane vertically
<Ctrl + b> %

### Split pane horizontally
<Ctrl + b> "

### Move the current pane left
<Ctrl + b> {

### Move the current pane right
<Ctrl + b> }

### Switch to pane to the direction
<Ctrl + b> <up-arrow>
<Ctrl + b> <down-arrow>
<Ctrl + b> <left-arrow>
<Ctrl + b> <right-arrow>

### Toggle synchronize-panes(send command to all panes)
:setw synchronize-panes

### Toggle between pane layouts 
<Ctrl + b> <Spacebar>

### Switch to next pane 
<Ctrl + b> o

### Show pane numbers
<Ctrl + b> q

### Switch/select pane by number
<Ctrl + b> q 0...9

### Toggle pane zoom
Ctrl + b z

### Convert pane into a window
<Ctrl + b> !



